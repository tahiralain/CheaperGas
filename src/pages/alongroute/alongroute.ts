import { AutocompletePage } from './../autocomplete/autocomplete';
import { Http } from '@angular/http';
import { Component } from '@angular/core';
import { NavController, MenuController, LoadingController, ModalController } from 'ionic-angular';
import { GoogleMap, GoogleMapsEvent, LatLng } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
declare var google;

@Component({
  selector: 'alongroute',
  templateUrl: 'alongroute.html'
})
export class AlongRoute {

//map: GoogleMap;
currPostionLat: any;
currPostionLong: any;
locationName: any;
destinationName: any;
address;
// originLat;
// originLong;
destinationLat: any;
destinationLong: any;
map: any;
mapDirections: any;
originMarkersData: any;
destinationMarkersData: any;
mapRoute: any;
originName: any;
destination_Name: any;

  constructor(public navCtrl: NavController, private menuCtrl: MenuController, private geolocation: Geolocation, private http: Http, private loadingCtrl: LoadingController, private modalCtrl: ModalController, private nativeGeocoder: NativeGeocoder) {

    this.address = {
      place: ''
    };

    // this.currPostionLat = 41.645187;
    // this.currPostionLong = -88.115836;

      this.geolocation.getCurrentPosition().then((resp) => {
      this.currPostionLat = resp.coords.latitude;
      this.currPostionLong = resp.coords.longitude;
      
      console.log("Lat: " + this.currPostionLat + " Long: " + this.currPostionLong);

      //this.loadMap();

       var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        this.map = new google.maps.Map(document.getElementById('map'), {
          zoom: 14,
          center: {lat: this.currPostionLat, lng: this.currPostionLong}
          //center: {lat: this.originLat, lng: this.originLong}
        });

        directionsDisplay.setMap(this.map);

      this.getAddressFromLatLong();

      }).catch((error) => {
        console.log('Error getting location', error);
      });
  }

  addMarkerToMap(latitude: any, longitude: any, stationName: any, stationAddress: any, dieselPrice: any, regPrice: any, prePrice: any, midPrice:any)
  {

    console.log("Marker: " + stationName);

var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="firstHeading" class="firstHeading">'+ stationName +'</h1>'+
      '<div id="bodyContent">'+
      '<p>Address: '+ stationAddress +'</p>'+
      '<p>Diesel: $'+ dieselPrice +'</p>'+
      '<p>Regular: $'+ regPrice +'</p>'+
      '<p>Premium: $'+ prePrice +'</p>'+
      '<p>Mid-Grade: $'+ midPrice +'</p>'+
      '</div>'+
      '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
      });

    var myLatLng = {lat: latitude, lng: longitude};

    //var image = 'https://cdn3.iconfinder.com/data/icons/map/500/gasstation-32.png';
    var image = 'http://cheaper-gas.us/Cheaper_app/yellow-marker.png';

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: this.mapDirections,
          title: 'Hello World!',
          icon: image
        });
    
        marker.addListener('click', function() {
          infowindow.open(this.mapDirections, marker);
        });
  }




  // getAllGasStationMarkers(originLat: any, originLong: any, destLat: any, destLong: any)
  getAllGasStationMarkers(originLat: any, originLong: any)
  {
      var originAPIURL = 'http://api.mygasfeed.com/stations/radius/'+ originLat +'/'+ originLong +'/1/reg/distance/fyspbx5ktx.json?';
      //var destinationAPIURL = 'http://api.mygasfeed.com/stations/radius/'+ destLat +'/'+ destLong +'/2/reg/distance/fyspbx5ktx.json?';

      this.http.get(originAPIURL).map(res => res.json()).subscribe(data => {
        this.originMarkersData = data.stations;
        console.log(originAPIURL);
        console.log("Origin Data: " + data.stations);
        console.log("Origin Length: " + data.stations.length);

        for(var i = 0; i < data.stations.length; i++)
          {
            let toArray =  data.stations[i].distance.split(" ");
            if(toArray[0] < 0.5) {
              console.log(toArray[0]);
              this.addMarkerToMap(parseFloat(data.stations[i].lat), parseFloat(data.stations[i].lng), data.stations[i].station, data.stations[i].address, data.stations[i].diesel_price, data.stations[i].reg_price, data.stations[i].pre_price, data.stations[i].mid_price);
            }
          }

    });
  }

showAddressModal () {
    let modal = this.modalCtrl.create(AutocompletePage);
    //let me = this;
    modal.onDidDismiss(data => {
      this.address.place = data;
      console.log(data);
      this.destinationName = this.address.place;
      let Destination_NAME = this.address.place.replace(/ /gi, "%20");

      var apiURL = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + Destination_NAME + '&key=AIzaSyDVOCdZnuNiXQkN48Wm6VVyXnqqmearRfI';
            this.http.get(apiURL).map(res => res.json()).subscribe(data => {
              console.log(apiURL);
              console.log(data.results[0].geometry.location);
              this.destinationLat = data.results[0].geometry.location.lat;
              this.destinationLong = data.results[0].geometry.location.lng;
              console.log(this.destinationLat + ' - ' + this.destinationLong);

              this.getDirections(this.locationName, Destination_NAME);

              //this.getAllGasStationMarkers(this.originLat, this.originLong, this.destinationLat, this.destinationLong)

            });

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        this.mapDirections = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
          center: {lat: this.currPostionLat, lng: this.currPostionLong}
        });
        directionsDisplay.setMap(this.mapDirections);
        
      directionsService.route({
          origin: this.locationName,
          destination: this.address.place,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
            this.mapRoute = response;
            console.log(this.mapRoute);
            
            this.locLat = response.routes[0].legs[0].steps[2].end_location.lat; 
            this.locLng = response.routes[0].legs[0].steps[2].end_location.lng; 

            console.log(this.locLat);
            console.log(this.locLng);

          } else {
            window.alert('Directions request failed due to ' + status);
          }

        });
    });
    modal.present();
  }

getDirections(originLoc: any, destinationLoc: any)
  {

    let loaderGasStations = this.loadingCtrl.create({
        content: "Getting Gas Stations Along Your Route..."
      });
      loaderGasStations.present();

    let origin = originLoc.replace(/ /gi, "+");
    let destination = destinationLoc.replace(/ /gi, "+");

    let APIURL = 'https://maps.googleapis.com/maps/api/directions/json?origin=' + origin + '&destination=' + destination + '&key=AIzaSyDVOCdZnuNiXQkN48Wm6VVyXnqqmearRfI';

    console.log("Directions URL: " + APIURL);

    this.http.get(APIURL).map(res => res.json()).subscribe(data => {
      let directions = data;  
      console.log("Directions Data: ");
      console.log(directions.routes[0].legs[0].steps[2].end_location.lat);

      let array_length = directions.routes[0].legs[0].steps.length;

      for(let i = 0; i < array_length; i++) {
        console.log(directions.routes[0].legs[0].steps[i].end_location);

        this.getAllGasStationMarkers(directions.routes[0].legs[0].steps[i].end_location.lat, directions.routes[0].legs[0].steps[i].end_location.lng);
      }

      loaderGasStations.dismiss();

    });
  }


getAddressFromLatLong(){
let loader = this.loadingCtrl.create({
        content: "Getting Your Current Location..."
      });
      loader.present();

  this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+ this.currPostionLat +','+ this.currPostionLong +'&key=AIzaSyDVOCdZnuNiXQkN48Wm6VVyXnqqmearRfI').map(res => res.json()).subscribe(data => {
        this.locationName = data.results[0].formatted_address;

        var newstr = this.locationName.replace(/ /gi, "+"); 

        console.log("Address Is: " + newstr);

        loader.dismiss();
    });

}

  OnOpenMenu()
  {
    this.menuCtrl.open();
  } 

loadMap(){
 
        //let location = new LatLng(this.currPostionLat,this.currPostionLong);
        
        let location = new LatLng(41.645187,-88.115836);
 
        this.map = new GoogleMap('map', {
          'backgroundColor': 'white',
          'controls': {
            'compass': true,
            'myLocationButton': true,
            'indoorPicker': true,
            'zoom': true
          },
          'gestures': {
            'scroll': true,
            'tilt': true,
            'rotate': true,
            'zoom': true
          },
          'camera': {
            'latLng': location,
            'tilt': 30,
            'zoom': 15,
            'bearing': 50
          }
        });
 
        this.map.on(GoogleMapsEvent.MAP_READY).subscribe(() => {
            console.log('Map is ready!');
        });
    }
}
