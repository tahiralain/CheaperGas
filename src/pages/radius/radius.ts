import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';


@Component({
  selector: 'radius',
  templateUrl: 'radius.html'
  

})
export class radiusPage {
   


  constructor(public navCtrl: NavController, private menuCtrl: MenuController, public alerCtrl: AlertController) {


  }
OnOpenMenu()
  {
    this.menuCtrl.open();
   }
//  gotonextpage(){
//      this.navCtrl.push(CardPage);
//  }

  doConfirm() {
    let confirm = this.alerCtrl.create({
      title: 'Want more Stations?',
      message: 'You can select upto 10 gas stations to appear along your route for only $1.99/year. Would you like these additional gas stations?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('No clicked');
          }
        }
      ]
    });
    confirm.present()
  }

}
