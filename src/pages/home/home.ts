import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { SignUp } from '../signup/signup';
import { HomeScreen } from '../homescreen/homescreen'
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

email:String = '';
password:String = '';

private apiUrl = 'http://cheaper-gas.us/Cheaper_app/login.php';

  constructor(private fb: Facebook ,public navCtrl: NavController, public alertCtrl: AlertController, public loadingCtrl:LoadingController, public http: Http) {
  }

  signUpPage() {
    this.navCtrl.push(SignUp);
  }

FacebookLogin(){
  
  let permissions = new Array<string>();
    let nav = this.navCtrl;
	let env = this;
    //the permissions your facebook app needs from the user
    permissions = ["public_profile", "user_friends", "email"];

    this.fb.login(['public_profile', 'user_friends', 'email'])
    .then(function(response){
      let userId = response.authResponse.userID;
      let params = new Array<string>();

      //Getting name and gender properties
      env.fb.api("/me?fields=name,gender", params)
      .then(function(user) {
        console.log("FB User Details: "  + user.name + " - " + user.gender + " - " + user.email);
        user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
        //now we have the users info, let's save it in the NativeStorage
      })
    }, function(error){
      console.log(error);
    });

}

  homeScreen() {
    if(this.email === '' || this.password === '') {
        let alert = this.alertCtrl.create({
          title:'Login Error', 
          subTitle:'All fields are rquired',
          buttons:['OK']
        });
        alert.present();
        return;
      }

let loader = this.loadingCtrl.create({
        content: "Logging in..."
      });
      loader.present();

    var data = {email: this.email, pwd: this.password};
    console.log(data);


this.http.post(this.apiUrl, data)
      .subscribe(data => {
        console.log(data);
        var jsonData = JSON.parse(data["_body"]);
        console.log("Length Is: " + jsonData.length);

        loader.dismissAll();

      if(jsonData.length <= 4){
        let alert = this.alertCtrl.create({
          title:'Login Response', 
          subTitle:'Invalid Email OR Password',
          buttons:['OK']
        });
        alert.present();
      } else {
        // let alert = this.alertCtrl.create({
        //   title:'Login Response', 
        //   subTitle:'Login Successful!',
        //   buttons:['OK']
        // });
        // alert.present();

        this.navCtrl.push(HomeScreen);
      }

    }, error => {
        console.log(error);// Error getting the data

   let alert = this.alertCtrl.create({
          title:'Login Response', 
          subTitle:data.toString(),
          buttons:['OK']
        });
        alert.present();

        loader.dismissAll();
      });

        this.navCtrl.push(HomeScreen);
  }
}
