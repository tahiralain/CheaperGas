import { InYourAreaPage } from './../in-your-area/in-your-area';
import { AlongRoute } from './../alongroute/alongroute';
import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';

@Component({
  selector: 'homescreen',
  templateUrl: 'homescreen.html'
})
export class HomeScreen {

  constructor(public navCtrl: NavController, private menuCtrl: MenuController) {

  }

  settingsPage() {
    this.navCtrl.push(AlongRoute);
  }

  InYourArea() {
    this.navCtrl.push(InYourAreaPage);
  }

  OnOpenMenu()
  {
    this.menuCtrl.open();
  } 

}
