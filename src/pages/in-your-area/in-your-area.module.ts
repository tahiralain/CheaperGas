import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InYourAreaPage } from './in-your-area';

@NgModule({
  declarations: [
    InYourAreaPage,
  ],
  imports: [
    IonicPageModule.forChild(InYourAreaPage),
  ],
  exports: [
    InYourAreaPage
  ]
})
export class InYourAreaPageModule {}
