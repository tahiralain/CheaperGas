import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { LaunchNavigator } from 'ionic-native';
import { Geolocation } from '@ionic-native/geolocation';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-in-your-area',
  templateUrl: 'in-your-area.html',
})
export class InYourAreaPage {

  markersData: any;
  num: any;
  markersData_Distance: any;
  currPostionLat: any;
  currPostionLong: any;

  discuss: string = "Sort by";
  isAndroid: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private menuCtrl: MenuController, public http: Http, private loadingCtrl: LoadingController, private geolocation: Geolocation) {
    this.num = 0;

    // this.currPostionLat = 41.645187;
    // this.currPostionLong = -88.115836;

    this.geolocation.getCurrentPosition().then((resp) => {
        this.currPostionLat = resp.coords.latitude;
        this.currPostionLong = resp.coords.longitude;
      
      console.log("Lat: " + this.currPostionLat + " Long: " + this.currPostionLong);

let loader = this.loadingCtrl.create({
        content: "Loading Gas Stations Near You..."
      });
      loader.present();

  this.http.get('http://api.mygasfeed.com/stations/radius/' + this.currPostionLat + '/' + this.currPostionLong + '/4/reg/price/fyspbx5ktx.json?').map(res => res.json()).subscribe(data => {
        this.markersData = data.stations;
    });

  this.http.get('http://api.mygasfeed.com/stations/radius/' + this.currPostionLat + '/' + this.currPostionLat + '/4/reg/distance/fyspbx5ktx.json?').map(res => res.json()).subscribe(data => {
        this.markersData_Distance = data.stations;
        console.log(this.markersData_Distance);
        loader.dismiss();
    });

      }).catch((error) => {
        console.log('Error getting location', error);
      });

  }

  ngAfterViewInit() {
}

OpenNavigation(address: string){ 
  console.log(address); 
  LaunchNavigator.navigate(address);
}

OnOpenMenu()
  {
    this.menuCtrl.open();
  }     


}
