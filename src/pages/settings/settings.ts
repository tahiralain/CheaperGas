import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { distancePage } from '../distance/distance'
import { FuelType } from '../fueltype/fueltype'
import { AlongRoute } from '../alongroute/alongroute'
import { SideNav } from '../sidenav/sidenav'
import { HomePage } from '../home/home'

@Component({
  selector: 'settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  constructor(public navCtrl: NavController, private menuCtrl: MenuController) {

  }

  logout() {
    this.navCtrl.push(HomePage);
  }
  // distancePage() {
    // this.navCtrl.push(DistancePage)

  // }
  // fuelTypePage() {
    // this.navCtrl.push(FuelType)

  // }
  // alongRoutePage() {
    // this.navCtrl.push(AlongRoute)
  // }

  // sideNavMenu() {
    // this.navCtrl.push(SideNav)
  // }

  OnOpenMenu()
  {
    this.menuCtrl.open();
  }
}
