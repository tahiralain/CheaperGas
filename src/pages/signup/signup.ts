import { HomePage } from './../home/home';
import { Http } from '@angular/http';
import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';


@Component({
  selector: 'signup',
  templateUrl: 'signup.html'
})
export class SignUp {

  username: String = '';
  email: String = '';
  password: String = '';
  confirmPassword: String = '';

  private apiUrl = 'http://cheaper-gas.us/Cheaper_app/usersignup.php';

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public loadingCtrl:LoadingController, public http: Http) {

  }

  HomePage() {
    this.navCtrl.pop(SignUp);
  }

  signUp() {

if(this.username === '' ||  this.email === '' || this.password === '' || this.confirmPassword === '') {
        let alert = this.alertCtrl.create({
          title:'Signup Error', 
          subTitle:'All fields are rquired',
          buttons:['OK']
        });
        alert.present();
        return;
      }

      if(this.password !== this.confirmPassword) {
        let alert = this.alertCtrl.create({
          title:'Signup Error', 
          subTitle:'Passwords do not match!',
          buttons:['OK']
        });
        alert.present();
        return;
      }

let loader = this.loadingCtrl.create({
        content: "Signing Up..."
      });
      loader.present();

    var data = {fullname:this.username, email: this.email, password: this.password};
    
    console.log(data);

this.http.post(this.apiUrl, data)
      .subscribe(data => {
        console.log(data);
        var jsonData = JSON.parse(data["_body"]);
        console.log("Length Is: " + jsonData.length);

        loader.dismissAll();

        this.navCtrl.push(HomePage);

    }, error => {
        console.log(error);// Error getting the data

   let alert = this.alertCtrl.create({
          title:'Signup Response', 
          subTitle:data.toString(),
          buttons:['OK']
        });
        alert.present();

        loader.dismissAll();
      });
  }
}
