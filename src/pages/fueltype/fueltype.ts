import { Component } from '@angular/core';
import { NavController,MenuController } from 'ionic-angular';

@Component({
  selector: 'fueltype',
  templateUrl: 'fueltype.html'
})
export class FuelType {

  constructor(public navCtrl: NavController,private menuCtrl: MenuController) {

  }

OnOpenMenu()
  {
    this.menuCtrl.open();
  } 
}
