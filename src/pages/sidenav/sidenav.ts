import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


@Component({
  selector: 'sidenav',
  templateUrl: 'sidenav.html'
})
export class SideNav {

  constructor(public navCtrl: NavController) {

  }

HomePage(){
  this.navCtrl.popToRoot();
}
}
