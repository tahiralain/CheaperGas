import { varstationPage } from './../pages/varstation/varstation';
import { radiusPage } from './../pages/radius/radius';
import { mindistancePage } from './../pages/mindistance/mindistance';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SignUp } from '../pages/signup/signup';
import { HomeScreen } from '../pages/homescreen/homescreen'
import { SettingsPage } from '../pages/settings/settings';
import { distancePage } from '../pages/distance/distance';
import { FuelType } from '../pages/fueltype/fueltype';
import { AlongRoute } from '../pages/alongroute/alongroute';
import { SideNav } from '../pages/sidenav/sidenav'
import { InYourAreaPage } from './../pages/in-your-area/in-your-area';
import { AutocompletePage } from './../pages/autocomplete/autocomplete';
import { Facebook } from '@ionic-native/facebook';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignUp,
    HomeScreen,
    SettingsPage,
    distancePage,
    FuelType,
    AlongRoute,
    SideNav,
    InYourAreaPage,
    AutocompletePage,
    mindistancePage,
    radiusPage,
    varstationPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignUp,
    HomeScreen,
    SettingsPage,
    distancePage,
    FuelType,
    AlongRoute,
    SideNav,
    InYourAreaPage,
    AutocompletePage,
    mindistancePage,
    radiusPage,
    varstationPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpModule,
    Geolocation,
    NativeGeocoder,
    Facebook,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
