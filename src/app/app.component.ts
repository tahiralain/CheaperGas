import { radiusPage } from './../pages/radius/radius';
import { mindistancePage } from './../pages/mindistance/mindistance';
import { varstationPage } from './../pages/varstation/varstation';
import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import {SignUp} from '../pages/signup/signup';
import {HomeScreen} from '../pages/homescreen/homescreen';
import {SettingsPage} from '../pages/settings/settings';
import {distancePage} from '../pages/distance/distance';
import {FuelType} from '../pages/fueltype/fueltype';
import {AlongRoute} from '../pages/alongroute/alongroute';
import {SideNav} from '../pages/sidenav/sidenav'
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;
  homeScreen = HomeScreen; 
  settingsPage = SettingsPage; 
  fuelType = FuelType;
  distancePage = distancePage;
  viableStations = varstationPage;
  MinDistancePage = mindistancePage;
  RadiusPage = radiusPage;

  
  
  
  @ViewChild('nav') nav: NavController;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private menuCtrl: MenuController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  OnLoad(page: any)
  {
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }

}

